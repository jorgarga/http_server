#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

#define PORT 8080
#define MAX_CONNECTIONS 10
#define MAX_BUFFER_SIZE 10000

int main(int argc, char const *argv[]) {
    int server_socket, answering_socket;
    int bind_result, listen_result;
    long valread;
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    int option = 1;
    char *response = "HTTP/1.1 200 OK\nContent-Type: text/plain\nContent-Length: 12\n\rHello world!";

    // Creating the main socket. This socket is the one which will
    // listen on our IP:PORT.
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket == 0){
        perror("In socket");
        exit(EXIT_FAILURE);
    }

    // The address/TCP port will be reusable when the server dies.
    setsockopt(server_socket,
               SOL_SOCKET,
               SO_REUSEADDR,
               &option,
               sizeof(option));

    // Initialize the socket address structure
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);
    memset(address.sin_zero, '\0', sizeof address.sin_zero);

    // Bind/Connect the server socket with the address structure.
    bind_result = bind(server_socket,
                       (struct sockaddr *) &address,
                       sizeof(address));
    if (bind_result < 0) {
        perror("In bind");
        exit(EXIT_FAILURE);
    }

    // And start listening.
    listen_result = listen(server_socket, MAX_CONNECTIONS);
    if (listen_result < 0) {
        perror("In listen");
        exit(EXIT_FAILURE);
    }

    while(1) {
        printf("*** Waiting for new connection ***\n");

        // The server socket is listening. When an incoming request
        // appears the answering socket is created.
        answering_socket = accept(server_socket,
                                  (struct sockaddr *) &address,
                                  (socklen_t*) &addrlen);
        if (answering_socket < 0) {
            perror("In accept");
            exit(EXIT_FAILURE);
        }

        // Possible security problem, I guess.
        // a buffer overflow could happen here.
        char buffer[MAX_BUFFER_SIZE] = {0};

        // Read the request and save the content in the buffer.
        valread = read(answering_socket , buffer, 30000);
        printf("Request content:\n%s\n\n", buffer);

        // Send the answer back to the client.
        write(answering_socket , response , strlen(response));

        printf("*** Response message sent ***\n");

        // Close the socket opened to answer this request.
        close(answering_socket);
    }

    return 0;
} 
