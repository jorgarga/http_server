# HTTP Server

The HTTP protocol is a text protocol implemented on top of TCP. This means that simple text -with special format- is sent over the wire using TCP. This repository shows a minimal implementation of an HTTP server.

## Why?
Wonderful excuse to code something in C, while **learning** (re-learning, actually) the very basics of how HTTP works.

## Instructions
The HTTP server will always answer 200 no matter what request is sent.

### Compilation and running
Compile:
> gcc -o server server.c

Running the server:
> ./server

The server will start listening on port 8080. This can be customized in the source code.
It will produce the following output on the screen:
> *** Waiting for new connection ***

Visit:
> http://127.0.0.1:8080

In the terminal one will see how the request looks like.

### Request:
```
GET / HTTP/1.1
Host: 127.0.0.1:8080
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0
Accept: image/webp,*/*
Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Connection: keep-alive
DNT: 1


```
In the request -which comes from the client- one can see:
* HTTP Method
* Requested URL
* Protocol version
* User-Agent
* Etc

### Response:
```
 HTTP/1.1 200 OK
 Content-Type: text/plain
 Content-Length: 12
 
 Hello world!
```
The response must include:
* Protocol version
* Content type
* Content length
* Content

The empty line between the response header and the content is **mandatory**.


# Future
As this is the smallest/simplest HTTP server implementation there is a lot of room for **improvement**.
One could implement... well, pretty much everything ;-) For example:
- Routing
- HTTP Verbs
- HTTP response codes
- Request headers (like encoding and MIME/types, to mention a couple)
- HTTPS


# More information about HTTP
The following links will help to gain deeper understanding:
- [HTTP (HyperText Transfer Protocol)](https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Basics.html)
- [An overview of HTTP](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview)
- [Everything you need to know to Build a simple HTTP server from scratch](https://medium.com/from-the-scratch/http-server-what-do-you-need-to-know-to-build-a-simple-http-server-from-scratch-d1ef8945e4fa
)
- [HTTP: Learn your browser's language!](https://jvns.ca/blog/2019/09/12/new-zine-on-http/)


# HTTP servers
- [Nginx](https://www.nginx.com/)
- [Cherokee](https://cherokee-project.com/)
- [Apache](https://www.apache.org/)